﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using EPiServer.Personalization.VisitorGroups;

namespace Alloy_Sample.Business
{
    [VisitorGroupCriterion(
        Category = "Technical",
        DisplayName = "Cookie Exists",
        Description = "Checks if a specific cookie exists")]

    public class CookieExistsCriterion : CriterionBase<CookieExistsCriterionSettings>
    {
        public override bool IsMatch(IPrincipal principal, HttpContextBase httpContext)
        {
            return httpContext.Request.Cookies[Model.CookieName] != null;
        }
    }
}