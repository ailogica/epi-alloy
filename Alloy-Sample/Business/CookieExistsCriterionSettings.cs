﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EPiServer.Personalization.VisitorGroups;

namespace Alloy_Sample.Business
{
    public class CookieExistsCriterionSettings : CriterionModelBase
    {
        [Required]
        public string CookieName { get; set; }

        public override ICriterionModel Copy()
        {
            return ShallowCopy();
        }
    }
}