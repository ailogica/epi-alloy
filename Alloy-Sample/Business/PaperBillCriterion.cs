﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using EPiServer.Personalization.VisitorGroups;

namespace Alloy_Sample.Business
{
    [VisitorGroupCriterion(
        Category = "Technical",
        DisplayName = "Paper Bill",
        Description = "Checks if user does not register with ebill")]

    public class PaperBillCriterion : CriterionBase<PaperBillCriterionSettings>
    {
        public override bool IsMatch(IPrincipal principal, HttpContextBase httpContext)
        {
            return true;
        }
    }
}